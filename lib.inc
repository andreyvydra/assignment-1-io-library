section .text
%define EXIT_SYSCALL 60
%define WRITE_SYSCALL 1
%define READ_SYSCALL 0
%define STDOUT 1
%define STDIN 0
%define ONECHAR 1
%define STRING_END 0
%define BASE 10
%define ASCII_ZERO 48
%define ASCII_NINE 57
%define SHIFT_FOR_CARRY 1
%define ASCII_MINUS 45
%define ASCII_SPACE 0x20
%define ASCII_NEWLINE 0xA
%define ASCII_TAB 0x9
%define TRUE 1
%define FALSE 0
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_SYSCALL
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    xor rcx, rcx

    .loop:
    mov al, byte[rdi + rcx]
    cmp al, STRING_END
    je .finish
    inc rcx
    jmp .loop
    
    .finish:
    mov rax, rcx
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push    rdi ; save data before calling
    call    string_length
    pop     rsi
    mov     rdx, rax ; quantity of letters
    mov     rax, WRITE_SYSCALL ; write
    mov     rdi, STDOUT ; to stdout
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push    rdi ; push to stack ascii letter, not POINTER!
    mov     rsi, rsp;
    mov     rdx, ONECHAR; one char
    mov     rax, WRITE_SYSCALL ; write
    mov     rdi, STDOUT ; to stdout
    syscall
    pop     rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, ASCII_NEWLINE
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbx
    mov rax, rdi
    mov rcx, BASE
    mov rbx, rsp
    dec rsp
    mov byte[rsp], STRING_END

    .loop:
    xor     rdx, rdx;
    div     rcx
    add     rdx, ASCII_ZERO
    dec     rsp
    mov     byte[rsp], dl
    test    rax, rax
    jnz     .loop

    mov rdi, rsp
    mov rsp, rbx
    sub rsp, 0x20 ; max decimal digit quantity of rax is 20
    call print_string
    add rsp, 0x20
    pop rbx
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    or rdi, rdi
    jge .positive

    .negative:
    push rdi
    mov rdi, ASCII_MINUS
    call print_char
    pop rdi
    neg rdi

    .positive:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .loop:
    mov al, [rsi]
    cmp al, [rdi]
    jnz .not_eq
    inc rsi
    inc rdi
    cmp al, STRING_END
    jnz .loop
    mov rax, TRUE
    ret

    .not_eq:
    mov rax, FALSE
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rdi, STDIN
    mov rax, READ_SYSCALL
    mov rdx, 1
    dec rsp
    lea rsi, [rsp]
    syscall
    test rax, rax
    jle .string_end
    mov al, [rsi]
    .string_end:
    inc rsp
    ret
     

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax
    push r12
    push r13
    push r15

    mov r12, rdi
    mov r15, rsi

    .space_loop: ; delete all spaces before the word
    call read_char
    cmp al, ASCII_SPACE
    je .space_loop
    cmp al, ASCII_NEWLINE
    je .space_loop
    cmp al, ASCII_TAB
    je .space_loop
    
    xor r13, r13
    dec r15 ; correction length buffer
    .word_loop:
    cmp al, ASCII_SPACE
    je .ok
    cmp al, ASCII_NEWLINE
    je .ok
    cmp al, ASCII_TAB
    je .ok
    test al, al ; if al is zero then stop reading
    jz .ok
    cmp r13, r15
    jg .buffer_overflow
    mov byte[r12 + r13], al
    inc r13
    call read_char
    jmp .word_loop

    .buffer_overflow:
    mov rax, FALSE
    jmp .end

    .ok:
    mov byte[r13 + r12], STRING_END
    mov rax, r12
    mov rdx, r13
    jmp .end

    .end:
    pop r15
    pop r13
    pop r12
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax ; result
    xor rcx, rcx ; loop count
    push r12
    push rbx
    xor r12, r12 ; current digit
    mov rbx, 10 ; decimal base

    .loop:
    mov r12b, byte[rdi + rcx]
    cmp r12b, ASCII_ZERO ; check for digit
    jl .end
    cmp r12b, ASCII_NINE
    jg .end
    sub r12b, ASCII_ZERO
    add rax, r12 ; add digit and do shift
    mul rbx
    inc rcx    
    jmp .loop

    .end:
    div rbx ; correction
    mov rdx, rcx
    pop rbx
    pop r12
    ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov al, byte[rdi]
    cmp al, ASCII_MINUS
    jz .parse_neg

    call parse_uint
    jmp .end
    
    .parse_neg:
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    
    .end:
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx

    .loop:
    cmp rax, rdx ; if string is bigger than buffer go to buffer
    jge .buffer_overflow
    mov cl, byte[rdi + rax]
    cmp cl, STRING_END
    jz .end
    mov byte[rsi + rax], cl
    inc rax
    jmp .loop

    .buffer_overflow:
    xor rax, rax
    ret

    .end:
    mov byte[rsi + rax], STRING_END ; add 0x0 to string ending
    inc rax
    ret
